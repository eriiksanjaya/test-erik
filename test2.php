<?php
	function trace ($data = null, $die = true) {
		echo "<pre>";
		print_r($data);
		echo "</pre>";

		if ($die) {
			die();
		}
	}

	// data awal
	$buah = array(
		array( "NAMA" => "JERUK", "HARGA" => 5000, "RASA" => "ASAM"),
		array( "NAMA" => "MANGGA", "HARGA" => 20000, "RASA" => "MANIS"),
		array( "NAMA" => "APEL", "HARGA" => 5000, "RASA" => "MANIS"),
		array( "NAMA" => "DURIAN", "HARGA" => 50000, "RASA" => "MANIS"),
		array( "NAMA" => "DUKU", "HARGA" => 10000, "RASA" => "ASAM"),
		array( "NAMA" => "NANAS", "HARGA" => 5000, "RASA" => "ASAM")
	);

	// trace($buah, false);

	// input data pisang
	$input['NAMA'] = "PISANG";
	$input['HARGA'] = "5000";
	$input['RASA'] = "MANIS";

	array_push($buah, $input); // masukkan ke variable buah

	foreach ($buah as $key => $item) {
		$nama[$key] = $item['NAMA'];
	}

	array_multisort($nama, SORT_DESC, $buah); // descending
	// trace($buah, false);
	$input_data = $buah;

	// 2 tampilkan huruf berisi K
	$contain = [];
	foreach ($buah as $key => $item) {
		if (strpbrk($item['NAMA'], 'K')) {
			$contain[] = $item;
		}
	}
	// trace($contain, false);


	// harga < 10000 dan rasa manis
	$less_than = [];
	foreach ($buah as $key => $item) {
		if ($item['HARGA'] < 10000 AND $item['RASA'] == 'MANIS') {
			$less_than[] = $item;
		}
	}
	// trace($less_than, false);
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Test</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-158001950-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-158001950-1');
	</script>

</head>
<body>
	<nav class="navbar navbar-expand-lg sticky-top navbar-dark bg-dark mb-4">
		<a class="navbar-brand" href="#">Test</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarNavAltMarkup">
			<div class="navbar-nav">
			</div>
		</div>
	</nav>

	<div class="container-fluid">
		<div class="col-md-8 mx-auto">
			<div class="row">
				<p>1.  Buat program untuk menambahkan buah Pisang yang harganya 5000 dan rasanya manis. ke variabel buah dan tampilkan Nama Buah dengan urutan secara descending</p>
			</div>
			<div class="row">
				<div class="card border-primary mb-2 mr-2 font-weight-light">
					<div class="card-header"> <h6>Input data pisang</h6> </div>
					<div class="card-body">
						$input['NAMA'] = 'PISANG'; <br>
						$input['HARGA'] = '5000'; <br>
						$input['RASA'] = 'MANIS'; <br><br>
						array_push($buah, $input); <br>
					</div>
				</div>

				<div class="card border-primary mb-2 mr-2 font-weight-light">
					<div class="card-header"> <h6>Descending</h6> </div>
					<div class="card-body">
						foreach ($buah as $key => $item) { <br>
							&nbsp;&nbsp;&nbsp;&nbsp;$nama[$key] = $item['NAMA']; <br>
						} <br><br>
						array_multisort($nama, SORT_DESC, $buah); <br>
					</div>
				</div>

				<div class="table-responsive">
					<table class='table table-bordered'>
						<thead class="thead-dark">
							<tr>
								<th class="text-center">No</th>
								<th>Nama</th>
								<th class="text-center">Harga</th>
								<th>Rasa</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($input_data as $key => $value) : ?>
								<tr>
									<td class="text-center"> <?php echo $key + 1; ?> </td>
									<td> <?php echo $value['NAMA']; ?> </td>
									<td class="text-right"> <?php echo "Rp " . number_format($value['HARGA']); ?> </td>
									<td> <?php echo $value['RASA']; ?> </td>
								</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
				</div>
			</div>

			<div class="row">
				<p>2.  Buat program untuk menampilkan Nama Buah yang nama buahnya mengandung Huruf "K"</p>
			</div>
			<div class="row">
				<div class="card border-primary mb-2 mr-2 font-weight-light">
					<div class="card-header"> <h6>Cari nama berisi huruf K</h6> </div>
					<div class="card-body">
						$contain = []; <br>
						foreach ($buah as $key => $item) { <br>
							&nbsp;&nbsp;&nbsp;&nbsp;if (strpbrk($item['NAMA'], 'K')) { <br>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$contain[] = $item; <br>
							&nbsp;&nbsp;&nbsp;&nbsp;} <br>
						} <br>
					</div>
				</div>

				<div class="table-responsive">
					<table class='table table-bordered'>
						<thead class="thead-dark">
							<tr>
								<th class="text-center">No</th>
								<th>Nama</th>
								<th class="text-center">Harga</th>
								<th>Rasa</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($contain as $key => $value) : ?>
								<tr>
									<td class="text-center"> <?php echo $key + 1; ?> </td>
									<td> <?php echo $value['NAMA']; ?> </td>
									<td class="text-right"> <?php echo "Rp " . number_format($value['HARGA']); ?> </td>
									<td> <?php echo $value['RASA']; ?> </td>
								</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
				</div>
			</div>

			<div class="row">
				<p>3.  Buat program untuk menampilkan Buah, yang harganya kurang dari 10.000 yang rasa nya Manis</p>
			</div>
			<div class="row">
				<div class="card border-primary mb-2 mr-2 font-weight-light">
					<div class="card-header"> <h6>Harga < 10.000 & Rasa Manis</h6> </div>
					<div class="card-body">
						$less_than = []; <br>
						foreach ($buah as $key => $item) { <br>
							&nbsp;&nbsp;&nbsp;&nbsp;if ($item['HARGA'] < 10000 AND $item['RASA'] == 'MANIS') { <br>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$less_than[] = $item; <br>
							&nbsp;&nbsp;&nbsp;&nbsp;} <br>
						} <br>
					</div>
				</div>
				<div class="table-responsive">
					<table class='table table-bordered'>
						<thead class="thead-dark">
							<tr>
								<th class="text-center">No</th>
								<th>Nama</th>
								<th class="text-center">Harga</th>
								<th>Rasa</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($less_than as $key => $value) : ?>
								<tr>
									<td class="text-center"> <?php echo $key + 1; ?> </td>
									<td> <?php echo $value['NAMA']; ?> </td>
									<td class="text-right"> <?php echo "Rp " . number_format($value['HARGA']); ?> </td>
									<td> <?php echo $value['RASA']; ?> </td>
								</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>


</body>
</html>
