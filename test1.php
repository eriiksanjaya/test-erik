<!DOCTYPE html>
<html lang="en">
<head>

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Test</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

</head>
<body>
	<nav class="navbar navbar-expand-lg sticky-top navbar-dark bg-dark mb-4">
		<a class="navbar-brand" href="#">Test</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarNavAltMarkup">
			<div class="navbar-nav">
			</div>
		</div>
	</nav>
</body>
</html>
<?php

	/*
		Nama 	: Erik Sanjaya
		Email 	: eriiksanjaya@gmail.com
		Ponsel 	: 089672057180
	*/

	function trace ($data = null, $die = true) {

		echo "<pre class=col-md-12>";
		print_r($data);
		echo "</pre>";

		if ($die) {
			die();
		}
	}

    function app_date_format($date, $format = 'd-m-Y', $from = 'Y-m-d') {

        $newdate = $date;
        
		$myDateTime = DateTime::createFromFormat($from, $date);
    	$newdate    = @$myDateTime->format($format);

        return $newdate;
    }

    function app_date_add($selectedTime = null, $modify = "+7 days", $format = 'Y-m-d' ) {

		$selectedTime = is_null($selectedTime) ? date($format) : $selectedTime;
		$datetime = DateTime::createFromFormat($format, $selectedTime);
		$datetime->modify($modify);

		return $datetime->format($format);
	}

	function app_date_diff($bigger, $smaller) {

        $bigger     = date_create($bigger);
        $smaller    = date_create($smaller);

        $diff = date_diff($smaller, $bigger);

        return $diff;
    }


    trace("-----------------------------------------", false);
    trace("Nama : Erik Sanjaya", false);
    trace("Email : eriiksanjaya@gmail.com", false);
    trace("Ponsel : 0896-7205-7180", false);
    trace("Link Git : <a href='https://gitlab.com/eriiksanjaya/test-erik'>git@gitlab.com:eriiksanjaya/test-erik.git</a>", false);
    trace("-----------------------------------------", false);


	/* praktek 1 */
	trace("<strong>Praktek 1</strong>", false);
	$angka = "1,3,5,7,9,11,13,15";
	trace("{$angka}", false);
	trace("i. tampilkan urutan angka di atas menggunakan fungsi perulangan", false);
	$angka = explode(",", $angka);
	$angka_loop = [];
	for ($i=0; $i < count($angka); $i++) { 
		$angka_loop[] = "angka ke-{$angka[$i]} <br>";
	}
	trace("Hasilnya ->", false);
	trace($angka_loop, false);




	/* praktek 2 */
	trace("<strong>Praktek 2</strong>", false);
	$tanggal = "2020-11-23";

	// jadikan 11-20-23
	trace("date {$tanggal}", false);
	trace("i. ubah format tanggal di atas menjadi <strong>11-20-23</strong>", false);
	$format_date_1 = app_date_format(date($tanggal), 'm-y-d');
	trace("Hasilnya -> {$format_date_1}", false);

	// tampilkan 6 bulan kedepan
	trace("ii. tampilkan 6 bulan kedepan dari tanggal di atas", false);
	$format_date_2 = app_date_add(date($tanggal), '+6 month');
	trace("Hasilnya -> {$format_date_2}", false);

	// tampilkan umur anda
	trace("iii. berapa umur anda jika dihitung dari tanggal di atas", false);
	$umur = app_date_diff(date($tanggal), date("1991-02-18"));
	$format_date_3 = "{$umur->y} Tahun, {$umur->m} Bulan, {$umur->d} Hari";
	trace("Hasilnya -> {$format_date_3}", false);




	/* praktek 3 */
	trace("<strong>Praktek 3</strong>", false);
	$array = [20,10,100,30,15,5];
	trace("Array [" . implode(",",$array) . "]", false);
	trace("i. urutkan array tersebut", false);

	sort($array);
	trace("Hasilnya ->", false);
	trace($array, false);

	trace("ii. tambahkan angka 120 di posisi terakhir pada array di atas", false);
	array_push($array, 120);
	trace("Hasilnya ->", false);
	trace($array, false);

	trace("iii. hapus angka 10 pada array di atas", false);
	unset($array[1]);
	trace("Hasilnya ->", false);
	trace($array, false);



	/* praktek 4 */
	trace("<strong>Praktek 4</strong>", false);
	$string = "aku suka makan nasi padang";
	$word_count = str_word_count($string);
	$to_array = explode(" ", $string);
	trace($string, false);

	foreach ($to_array as $key => $value) {
		$val = $to_array;
		$asc[] = implode(" ", array_splice($val, 0, $key+1));

		$val = $to_array;
		krsort($val);
		$desc[] = implode(" ", array_splice($val, 0, $key+1));
	}

	$merge = array_merge($asc, $desc);
	$result = '["'.implode('","', $merge).'"]';

	trace('i. buatlah algoritma dengan memanfaatkan perulangan yang menghasilkan Array dengan result seperti berikut : <br> ["aku","aku suka","aku suka makan","aku suka makan nasi","aku suka makan nasi padang","padang","padang nasi","padang nasi makan","padang nasi makan suka","padang nasi makan suka aku"]
		', false);


	trace("Hasilnya ->", false);
	trace($result,false);

	trace("Hasil lainnya ->", false);
	trace(array_merge($asc, $desc),false);

?>